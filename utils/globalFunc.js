var gcm = require('node-gcm');
var underscore = require('underscore');
module.exports = function (){
	this.sendPushNotification = function (devicesList,title,message){
		var message = new gcm.Message();
		message.addNotification({
		  title: title,
		  body: message,
		  icon: 'ic_launcher'
		});
		// Set up the sender with you API key 
		var sender = new gcm.Sender('insert Google Server API Key here');
		message.addData('key1',message);
		// Add the registration tokens of the devices you want to send to  ["token1","token2"]
		var registrationTokens = [];
		if(underscore.isArray(devicesList)){
			registrationTokens = devicesList;
		}else{
			if(underscore.isString(devicesList))
			 	registrationTokens.push(devicesList); 
		}
		 
		// Send the message 
		// ... trying only once 
		sender.sendNoRetry(message, { registrationTokens: registrationTokens }, function(err, response) {
		  if(err) console.error(err);
		  else    console.log(response);
		});
	}
}