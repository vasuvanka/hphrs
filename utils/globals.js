var global = {
	'dbHost': 'localhost',
	'dbPort': 27017,
	'dbName': 'sampleDB',
	'appPort': '8080',
	'appVersion': 'v1',
	'secret':'hello world',
	'promoDuration':1 ,// in days
	'promoUseDuration':2, // in days
	'promoPercentage':true,
	'promoPrice':100,
	'isBackReferral': true,
	'searchDistance': 3000
};
module.exports = global;


/*

testMapApiKey = AIzaSyC626py0lKAYjTthZ5v7tAsVuVbKYgoq7k
*/