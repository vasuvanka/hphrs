var mongoose    = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;
/*Users table structure*/
exports.Users = mongoose.model('Users',{
	'created_at'	:{ type: Date, default: Date.now },
	'modified_at'	:{ type: Date, default: Date.now }, 
	'name'			:{ type: String, default: "NA" },
	'isFirst'		:{ type: Boolean, default: true }, 
	'mobile'		:{ type: Number, default: -1 }, 
	'email'			:{ type: String, default: "NA" }, 
	'password'		:{ type: String, default: "NA" },
	'salt'			:String,
	'lastLogin'		:Date,
	'lastPwdChange'	:Date,
	'type'			: { type: String, default: "user" },				
	'deviceId'		:{ type: String, default: "NA" },
	'imei'			:{ type: String, default: "NA" },
	'dob'			:{ type: Date, default: Date.now },
	'pictureId'		:{ type: String, default: "NA" },
	'isBlocked'		:{ type: Boolean, default: false },
	'isRemoved'		:{ type: Boolean, default: false },
	'token'			:{ type: String, default: "NA" },
	'hash'			:{ type: String, default: "NA" },
	'otp'			:{ type: String, default: "NA" },
	'referralCode'	:{ type: String, default: "NA" },
	'usedReferralCode':String,
	'geoType'		:String, //point
	'geo'			: { type: [Number], index: '2d' } //[latitude,langitude]
},'Users');

/*service provider table structure*/
exports.Providers = mongoose.model('Providers',{
	'created_at'	:{ type: Date, default: Date.now },
	'modified_at'	:{ type: Date, default: Date.now },
    'name'			:{ type: String, default: "NA" },
    'owner'			:[{ type: Schema.Types.ObjectId, ref: 'Users' }],
	'description'	:{ type: String, default: "NA" },
	'location'		:{ type: String, default: "NA" },
	'city'			:{ type: String, default: "NA" },
	'country'		:{ type: String, default: "NA" },
	'landmark'		:{ type: String, default: "NA" },
	'isSaloon'		:{ type: Boolean, default: false },
	'isFitness'		:{ type: Boolean, default: false },
	'aboutUs'		:{ type: String, default: "NA" },
	'help'			:{ type: String, default: "NA" },
	'isBlocked'		:{ type: Boolean, default: false },
	'isRemoved'		:{ type: Boolean, default: false },
	'pictureId'		:{ type: String, default: "NA" },
 	'rating'		:{ type: Number, default: -1 },
	'serviceCount'	:{ type: Number, default: -1 },
	'ratingCount'	:Number,
	'geoType'		:String, //point
	'geo'			: { type: [Number], index: '2d' } //[lng,lat]
},'Providers');
/*offers table structure*/
exports.Services = mongoose.model('Services',{
	'created_at'	:{ type: Date, default: Date.now },
	'modified_at'	:{ type: Date, default: Date.now },
	'name'			:{ type: String, default: "NA" },
	'isOffer'		:{ type: Boolean, default: false },
	'provider_id'	:[{ type: Schema.Types.ObjectId, ref: 'Providers' }],
	'description'	:{ type: String, default: "NA" },
	'pictureId'		:{ type: String, default: "NA" },
	'offerPrice'	:{ type: Number, default: -1 },
	'totalPrice'	:{ type: Number, default: -1 },
	'isPercentage'	:{ type: Boolean, default: false },
	'serviceType'	:{ type: String, default: "NA" },// (saloon or fitness)
	'isCombo'		:{ type: Boolean, default: false },
	'offerList'		:Array,
	'fromDate'		:Date,// (Offer valid from)
    'toDate'		:Date,// (Offer Valid To)
	'isRemoved'		:{ type: Boolean, default: false },
	'isExpired'		:{ type: Boolean, default: false },
	'isOnlyToday'	:{ type: Boolean, default: false },
	'slot'	 		:Array,
	'isEveryWeekDay':{ type: Boolean, default: false },
	'offerDayList'	:Array,
	'isEveryDay'	:{ type: Boolean, default: false },
	'rating'		:Number,
	'serviceCount'	:Number,
	'ratingCount'	:Number
},'Services');
/*offers table structure*/
exports.UserBookings = mongoose.model('UserBookings',{
	'created_at'	:{ type: Date, default: Date.now },
	'modified_at'	:{ type: Date, default: Date.now },
	'user_id'		:[{ type: Schema.Types.ObjectId, ref: 'Users' }],
	'provider_id'	:[{ type: Schema.Types.ObjectId, ref: 'Providers' }],
	'service_id'	:[{ type: Schema.Types.ObjectId, ref: 'Services' }],
	'isRemoved'		:{ type: Boolean, default: false },
	'isExpired'		:{ type: Boolean, default: false },
	'slot'			:{ type: String, default: "NA" },
	'status'		:{ type: String, default: "NA" }, // completed or cancelled or pending
	'bookingDate'	:Date,
	'serviceDate'	:Date,
	'totalPrice'	:{ type: Number, default: -1 },
	'offerPrice'	:{ type: Number, default: -1 },
	'transaction_id':{ type: String, default: "NA" },
    'isReferral'	:{ type: Boolean, default: false },
    'promocode'		:[ { type: String, default: "NA" , ref: 'UserPromos' }],
    'referral_id'	:[{ type: Schema.Types.ObjectId, ref: 'UserReferrals' }]
},'UserBookings');
exports.Feedbacks = mongoose.model('Feedbacks',{
	'created_at'	:{ type: Date, default: Date.now },
	'modified_at'	:{ type: Date, default: Date.now },
	'user_id'		:[{ type: Schema.Types.ObjectId, ref: 'Users' }],
	'provider_id'	:[{ type: Schema.Types.ObjectId, ref: 'Providers' }],
	'booking_id'	:[{ type: Schema.Types.ObjectId, ref: 'Boookings' }],
	'service_id'	:[{ type: Schema.Types.ObjectId, ref: 'Services' }],
	'isRemoved'		:{ type: Boolean, default: false },
	'rating'		:{ type: Number, default: -1 },
	'description'	:{ type: String, default: "NA" },
	'feedbackDate'	:{ type: Date, default: Date.now },
	'serviceDate'	:Date
},'Feedbacks');
exports.UserFavourites = mongoose.model('UserFavourites',{
	'created_at'	:{ type: Date, default: Date.now },
	'modified_at'	:{ type: Date, default: Date.now },
	'user_id'		:[{ type: Schema.Types.ObjectId, ref: 'Users' }],
	'provider_id'	:[{ type: Schema.Types.ObjectId, ref: 'Providers' }],
	'service_id'	:[{ type: Schema.Types.ObjectId, ref: 'Services' }],
	'isRemoved'		:{ type: Boolean, default: false },
	'priority'		:{ type: String, default: "NA" }
},'UserFavourites');
exports.UserReferrals = mongoose.model('UserReferrals',{
	'created_at'	:{ type: Date, default: Date.now },
	'modified_at'	:{ type: Date, default: Date.now },
	'ref_user_id'	:[{ type: Schema.Types.ObjectId, ref: 'Users' }],
	'user_id'		:[{ type: Schema.Types.ObjectId, ref: 'Users' }],
	'provider_id'	:[{ type: Schema.Types.ObjectId, ref: 'Providers' }],
	'service_id'	:[{ type: Schema.Types.ObjectId, ref: 'Services' }],
	'fromDate'		:Date,
	'toDate'		:Date,
	'promocode'		:[{ type: String, ref: 'UserPromos' }],
	'isRemoved'		:{ type: Boolean, default: false },
	'isUsed'		:{ type: Boolean, default: false },
	'isExpired'		:{ type: Boolean, default: false },
	'isReferral'	:{ type: Boolean, default: false },
	'isBackReferral'	:{ type: Boolean, default: false }
},'UserReferrals');
exports.UserPromos = mongoose.model('UserPromos',{
	'created_at'	:{ type: Date, default: Date.now },
	'modified_at'	:{ type: Date, default: Date.now },
	'promocode'		:String,
	'fromDate'		:Date,// (Offer valid from)
    'toDate'		:Date,// (Offer Valid To)
	'isRemoved'		:{ type: Boolean, default: false },
	'user_id'		:[{ type: Schema.Types.ObjectId, ref: 'Users' }],
	'provider_id'	:[{ type: Schema.Types.ObjectId, ref: 'Providers' }],
	'service_id'	:[{ type: Schema.Types.ObjectId, ref: 'Services' }],
	'isExpired'		:{ type: Boolean, default: false },
	'type'			:{ type: String, default: "multi" }, // multi / single
	'isPercentage'	:{ type: Boolean, default: false },
	'price'			:{ type: Number, default: -1 },
	'category'		:{ type: String, default: "NA" } //saloon or fitness
},'UserPromos');


