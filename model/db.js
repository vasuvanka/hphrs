var mongoose = require('mongoose');
var globals = require('../utils/globals');

var mongoUrl = "mongodb://"+globals.dbHost+":"+globals.dbPort+"/"+globals.dbName;
/* # connection establishment to db*/
mongoose.connect(mongoUrl,function(err, db){
  if(err)
    console.log(err)
  else{
    console.log("connected to db")
  }
});