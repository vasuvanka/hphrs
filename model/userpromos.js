var underscore = require('underscore');
function UserPromos(db){
	this.findOne = function(query,selectProp,callback){
		db.UserPromos.findOne(query,selectProp,function(err,doc){
			callback(err,doc)
		});
	}
	this.find = function(query,selectProp,sortProp,skipNum,limitNum,callback){
		var sort = underscore.isEmpty(sortProp) ?  {'created_at':-1} : sortProp ;
		var qobj = db.UserPromos.find(query,selectProp);
		qobj.sort(sortProp).skip(skipNum).limit(limitNum).exec(function(err,doc){
			callback(err,doc)
		});
	}
	this.save = function(obj,callback){
		var qobj = new db.UserPromos(obj);
		qobj.save(function(err,doc){
			callback(err,doc)
		});
	}
	this.update = function(query,updateProp,isMulti,callback){
		db.UserPromos.update(query,{$set:updateProp},{multi:isMulti},function (err,doc){
			callback(err,doc);
		});
	}
	this.findRef = function (query,refId,selectProp,sortProp,skipNum,limitNum,callback){
		var sort = underscore.isEmpty(sortProp) ?  {'created_at':-1} : sortProp ;
		var qobj = db.UserPromos.find(query,selectProp);
		qobj.sort(sortProp).skip(skipNum).limit(limitNum).populate(refId).exec(function(err,doc){
			callback(err,doc)
		});
	}
}
module.exports.UserPromos = UserPromos;