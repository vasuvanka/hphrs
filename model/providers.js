var underscore = require('underscore');
var globals = require("../utils/globals");
function Providers(db){
	this.findOne = function(query,selectProp,callback){
		db.Providers.findOne(query,selectProp,function(err,doc){
			callback(err,doc)
		});
	}
	this.find = function(query,selectProp,sortProp,skipNum,limitNum,callback){
		var sort = underscore.isEmpty(sortProp) ?  {'created_at':-1} : sortProp ;
		var qobj = db.Providers.find(query,selectProp);
		qobj.sort(sortProp).skip(skipNum).limit(limitNum).exec(function(err,doc){
			callback(err,doc)
		});
	}
	this.save = function(obj,callback){
		var qobj = new db.Providers(obj);
		qobj.save(function(err,doc){
			callback(err,doc)
		});
	}
	this.update = function(query,updateProp,isUpsert,callback){
		db.Providers.update(query,{$set:updateProp},{upsert:isUpsert},function (err,doc){
			callback(err,doc);
		});
	}
	this.count = function(query,updateProp,callback){
		db.Providers.update(query,{$inc:updateProp},function (err,doc){
			callback(err,doc);
		});
	}
	this.search = function(text,skip,limit,callback){
		db.Providers.
		  find({ $text : { $search : text } },
		    { score : { $meta: "textScore" } }).
		  sort({ rating: -1 ,score : { $meta : 'textScore' } }).
		  skip(skip).
		  limit(limit).
		  exec(function(err, docs) {
		    callback(err,docs)
		  });
	}
	this.geoSearch = function (latitude,longitude,skip,limit,callback){
		var distance = globals.searchDistance;
		db.Providers.find({'geo': {
			  	$near: [latitude,longitude],
			  	$maxDistance: distance
		  }}).skip(skip).limit(limit).exec(function (err, docs) {
			callback(err,docs);
		})
	}
}
module.exports.Providers = Providers;