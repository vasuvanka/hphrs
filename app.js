var express = require('express');
var logger = require('morgan');
var bodyParser = require('body-parser');
var compression = require('compression');
var adminRoutes = require('./routes/adminRoutes');
var providerRoutes = require('./routes/providerRoutes');
var userRoutes = require('./routes/userRoutes');
var globals = require('./utils/globals');
require('./model/db');

var app = express();

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(compression());
app.use('/profile', express.static(__dirname +'/files'));

const version = globals.appVersion;
const globalUri = "/api/"+version; 

/* basic routing */
app.use(function (req,res,next){
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.use(globalUri+'/admin/', adminRoutes);
app.use(globalUri+'/provider/', providerRoutes);
app.use(globalUri+'/user/', userRoutes);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});


// error handlers

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
    res.send( {
      data: [err.message],
      status: "error"
    });
});


module.exports = app;
