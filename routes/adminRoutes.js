var express = require('express');
var underscore = require('underscore');
// var crypto = require('crypto');
var db = require('../model/schema');
var usersModel = require('../model/users').Users;
var UserBookingsModel = require('../model/userbookings').UserBookings;
var ServicesModel = require('../model/services').Services;
var ProvidersModel = require('../model/providers').Providers;
var UserPromosModel = require('../model/userpromos').UserPromos;

var uid = require('uid');
var jwt = require('jsonwebtoken'); 
var multer = require("multer");
var bcrypt = require('bcryptjs');

var admin = express.Router();

var users = new usersModel(db);
var userbookings = new UserBookingsModel(db);
var services = new ServicesModel(db);
var userpromos = new UserPromosModel(db);
var providers = new ProvidersModel(db);


admin.post('/providers', function(req, res, next) {
	var data = req.body;
	users.findOne({'mobile':data.mobile,'type':'provider'},{},function(err,doc){
		if (err) {
			res.send({'status':'error','data':['something went wrong']})
		}else{
			if (doc == null) {
				var password = uid(10).toUpperCase();
				bcrypt.genSalt(10, function(err, salt) {
				    bcrypt.hash(password, salt, function(err, hash) {
				        if (err) {
				        	res.send({'status':'error','data':['something went wrong']})
				        }else{
							var obj = {
								'name'			:data.name, 
								'mobile'		:data.mobile, 
								'email'			:data.email, 
								'password'		:password,
								'salt'			:salt,
								'lastLogin'		:Date.now(),
								'lastPwdChange'	:Date.now(),
								'type'			:'provider',						
								'deviceId'		:data.deviceId || 'NA',
								'hash'			:hash
							};
							users.save(obj,function (err,docx){
								if (err) {
									res.send({'status':'error','data':["invalid data entry"]});
								}else{
									var objx ={
									    'name'		:data.partner.name,
									    'owner'		:docx._id,
										'description':data.partner.description,
										'location'	:data.partner.location,
										'city'		:data.partner.city,
										'country'	:data.partner.country,
										'landmark'	:data.partner.landmark,
										'isSaloon'	:data.partner.isSaloon,
										'isFitness'	:data.partner.isFitness,
										'aboutUs'	:data.partner.aboutUs,
										'help'		:data.partner.help,
										'geoType'	:data.partner.geoType || 'point', 
										'geo'		:data.partner.geo
									};
									providers.save(objx,function (err,result){
										if (err) {
											res.send({'status':'error','data':["invalid data entry"]});
										}else{
											res.send({'status':'success','data':result});
										}
									});
								}
							});
				        }
				    });
				});
			}else{
			var obj ={
				    'name'		:data.partner.name,
				    'owner'		:doc._id,
					'description':data.partner.description,
					'location'	:data.partner.location,
					'city'		:data.partner.city,
					'country'	:data.partner.country,
					'landmark'	:data.partner.landmark,
					'isSaloon'	:data.partner.isSaloon,
					'isFitness'	:data.partner.isFitness,
					'aboutUs'	:data.partner.aboutUs,
					'help'		:data.partner.help,
					'geoType'	:data.partner.geoType || 'point', 
					'geo'		:data.partner.geo
				};
				providers.save(obj,function (err,result){
					if (err) {
						res.send({'status':'error','data':["invalid data entry"]});
					}else{
						res.send({'status':'success','data':result});
					}
				});
			}
		}
	})
});
admin.get('/owner/providers', function(req, res, next) {
	var providerId = req.params.providerId;
	var skip =  parseInt(req.query.skip) || 0;
	var limit = parseInt(req.query.limit) || 20;
	users.find({"type":"provider"},{},{},skip,limit,function (err,docs){
		if (err || docs == null) {
			res.send({'status':'error','data':["Ooops something went wrong"]});
		}else{
			res.send({'status':'success','data':docs});
		}
	})
});
admin.get('/providers', function(req, res, next) {
	var providerId = req.params.providerId;
	var skip =  parseInt(req.query.skip) || 0;
	var limit = parseInt(req.query.limit) || 20;
	providers.find({},{},{},skip,limit,function (err,docs){
		if (err || docs == null) {
			res.send({'status':'error','data':["Ooops something went wrong"]});
		}else{
			res.send({'status':'success','data':docs});
		}
	})
});
admin.get('/providers/:providerId', function(req, res, next) {
	var providerId = req.params.providerId;
	providers.findOne({'_id':providerId},{},function (err,doc){
		if (err || doc == null) {
			res.send({'status':'error','data':["invalid provider id"]});
		}else{
			res.send({'status':'success','data':doc});
		}
	})
});
admin.put('/providers/:providerId', function(req, res, next) {
	var providerId = req.params.providerId;
	var data = req.body.data;
	if(underscore.isArray(data)){
		var obj = {};
		for (var i = 0; i < data.length; i++) {
			obj[data[i].name] = data[i].value;
		};
		providers.update({'_id':providerId},obj,false,function (err,doc){
			if (err) {
				res.send({'status':'error','data':["invalid provider id"]});
			}else{
				res.send({'status':'success','data':obj});
			}
		})
	}
});

// users
admin.get('/users', function(req, res, next) {
	var providerId = req.params.providerId;
	var skip =  parseInt(req.query.skip) || 0;
	var limit = parseInt(req.query.limit) || 20;
	users.find({},{},{},skip,limit,function (err,docs){
		if (err || docs == null) {
			res.send({'status':'error','data':["Ooops something went wrong"]});
		}else{
			res.send({'status':'success','data':docs});
		}
	})
});
admin.get('/users/:userId', function(req, res, next) {
	var userId = req.params.userId;
	users.findOne({'_id':userId},{},function (err,doc){
		if (err || doc == null) {
			res.send({'status':'error','data':["invalid provider id"]});
		}else{
			res.send({'status':'success','data':doc});
		}
	})
});
admin.put('/users/:userId', function(req, res, next) {
	var userId = req.params.userId;
	var data = req.body.data;
	if(underscore.isArray(data)){
		var obj = {};
		for (var i = 0; i < data.length; i++) {
			obj[data[i].name] = data[i].value;
		};
		users.update({'_id':userId},obj,false,function (err,doc){
			if (err) {
				res.send({'status':'error','data':["invalid provider id"]});
			}else{
				res.send({'status':'success','data':obj});
			}
		})
	}
});

// service

admin.get('/providers/:providerId/services', function(req, res, next) {
	var providerId = req.params.providerId;
	services.findOne({'_id':providerId},{},function (err,doc){
		if (err || doc == null) {
			res.send({'status':'error','data':["invalid provider id"]});
		}else{
			res.send({'status':'success','data':doc});
		}
	})
});
admin.put('/providers/:providerId/services/:serviceId', function(req, res, next) {
	var providerId = req.params.providerId;
	var serviceId = req.params.serviceId;
	var data = req.body.data;
	if(underscore.isArray(data)){
		var obj = {};
		for (var i = 0; i < data.length; i++) {
			obj[data[i].name] = data[i].value;
		};
		services.update({'_id':serviceId},obj,false,function (err,doc){
			if (err) {
				res.send({'status':'error','data':["invalid provider id"]});
			}else{
				res.send({'status':'success','data':doc});
			}
		})
	}
});
function generateJWT(data,callback){
	/*{ user_id: data._id }*/
	var token = jwt.sign({ user_id: data._id }, "lot of secret here",{ algorithm: 'HS256',expiresIn: '100 days' });
	console.log(token);
	callback(token);
}

module.exports = admin;
