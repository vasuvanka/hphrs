/**
* user dependecies and declarations are here
*/
var express = require('express');
var underscore = require('underscore');
// var crypto = require('crypto');
var globals = require('../utils/globals');
var db = require('../model/schema');
var usersModel = require('../model/users').Users;
var UserBookingsModel = require('../model/userbookings').UserBookings;
var ServicesModel = require('../model/services').Services;
var ProvidersModel = require('../model/providers').Providers;
var UserPromosModel = require('../model/userpromos').UserPromos;
var FeedbacksModel = require('../model/feedbacks').Feedbacks;
var UserFavouritesModel = require('../model/userfavourites').UserFavourites;
var UserReferralsModel = require('../model/userreferrals').UserReferrals;

var cc = require('coupon-code');

var uid = require('uid');
var jwt = require('jsonwebtoken'); 
var multer = require("multer");

var user = express.Router();

var users = new usersModel(db);
var userbookings = new UserBookingsModel(db);
var services = new ServicesModel(db);
var userpromos = new UserPromosModel(db);
var providers = new ProvidersModel(db);
var feedbacks = new FeedbacksModel(db);
var userfavourites = new UserFavouritesModel(db);
var userreferrals = new UserReferralsModel(db);

/* /api/v1/user/login */
user.post('/login', function(req, res, next) {
	/* {"mobile":""}*/
	if ( !underscore.isUndefined(req.body.mobile) ) {
		if (req.body.mobile.toString().length == 10) {
			users.findOne({'mobile':req.body.mobile},{},function (err,doc){
				if(err == null && doc == null){
					var otp = generateOtp();
					var obj = {"mobile":req.body.mobile,"otp" : otp};
						users.save(obj,function (err,doc){
						console.log(err+'----'+doc);
						res.send({'status':'success','data':obj});
					})
				}else{
					var otp = generateOtp();
					var objx = {'otp':otp};
					users.update({'mobile':req.body.mobile},objx,true,function (err,docx){
						console.log(err+'---'+JSON.stringify(docx));
						res.send({'status':'success','data':{"mobile":req.body.mobile,"otp" : otp}});
					});
				}
			})
		}else{
			var obj = {status:"error",data:["invalid mobile Number"]};
			res.send(obj);		
		}
	}else{
		var obj = {status:"error",data:["invalid mobile Number"]};
		res.send(obj);
	}
});
user.post('/validate', function(req, res, next) {
	/* {"mobile":"","otp":"","imei":"","deviceId":"","promo":"",name:""}*/
	if ( req.body.mobile != undefined && req.body.otp != undefined) {
		/* API call for OTP here */
			var mobile = req.body.mobile.toString().length == 10 ? req.body.mobile : null;
			var otp = req.body.otp.toString().length == 4 ? req.body.otp : null;
			if (mobile != null && otp != null) {
				users.findOne({'mobile':mobile,'otp':otp},{},function (err,doc){
					console.log(err+'---'+JSON.stringify(doc));
				 	if (err && doc == null) {
						var obj = {status:"error",data:["invalid Access"]};
						res.send(obj);
				 	}else{
				 		if (underscore.isNull(doc)) {
							var obj = {status:"error",data:["invalid operation"]};
							res.send(obj);
				 		}else{
				 			generateJWT(doc._id,function (token){
				 				var objx = {'token':token,'lastLogin':Date.now(),'imei':req.body.imei,
				 							'deviceId':req.body.deviceId,'usedReferralCode':req.body.promo};
				 				if (doc.referralCode == "NA") {
				 					generateCouponCode(function (coupon){
				 						objx['referralCode'] = coupon;
				 					});
				 				}
				 				validatePromo(doc,objx);
								users.update({'mobile':req.body.mobile},objx,false,function (err,docx){
									console.log(err+'---'+docx);
									var obj ={
										'id':doc._id,
										'mobile':doc.mobile,
										'pictureId':doc.pictureId,
										'token':token
									};
									res.send({'status':'success','data':obj});
								});
							});
				 		}
				 	}
				 });
			}else{
				var obj = {status:"error",data:["something went wrong"]};
				res.send(obj);
			}
	}else{
		var obj = {status:"error",data:["invalid otp"]};
		res.send(obj);
	}
});

/* user headers validation */
user.use(function (req, res, next) {
	var userid = req.get('app_user_id') || req.headers.app_user_id || null;
	var token = req.get('token') || req.headers.token || null;
	if( userid != null && token != null){
		next();
	}else{
		var err = new Error("Unauthorized Access or missing app_user_id, token");
		err.status = 404;
		next(err);
	}	
});

user.use(function (req, res, next) {
	/* Bearer token*/
	var authToken = req.get('token') ;
	var token = authToken.split(" ")[1];
	jwt.verify(token,require('../utils/globals').secret, function(err, decoded) {
		if(decoded != undefined && decoded != null){
			 users.findOne({'_id':decoded.id,'token':token},{},function (err,doc){
			 	if (err || doc == null) {
					var err = new Error('Unauthorized Access');
					err.status = 401;
					next(err);
			 	}else{
			 		next();
			 	}
			 });
		}else{
			var err = new Error('Unauthorized Access');
			err.status = 401;
			next(err);
		}
	});
});
	/*file upload*/
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './files/uploads')
  },
  filename: function (req, file, cb) {
    cb(null, req.get('app_user_id') + Date.now())
  }
});
	 
var upload = multer({ storage: storage });

user.get('/:userid/profile', function (req, res, next) {
	var id = req.get('app_user_id') ;
	var userid = req.params.userid ;
	users.findOne({'_id':id},{"pictureId":1},function (err,doc){
		var obj = {};
		if (err && doc == null) {
			obj.status = "error";
			obj.data = ["invalid user id"];
		}else{
			var data = { "pictureId" : doc.pictureId };
			obj.status = "success";
			obj.data = data;
		}
		res.send(obj);
	});
});

user.post('/:userid/profile', upload.single('file'), function (req, res, next) {
	var id = req.get('app_user_id') ;
	var userid = req.params.userid ;
		var obj = {
			'pictureId':req.file.filename
		};
		users.update({'_id':id},obj,false,function (err,doc){
			var obj = {};
				if (err && doc == null) {
					obj.status = "error";
					obj.data = ["invalid user id"];
				}else{
					var data = { "pictureId" : req.file.filename };
					obj.status = "success";
					obj.data = data;
				}
				res.send(obj);
		});

});
/* rename */
user.put('/:userId/rename',function (req , res, next){
	var id =  req.params.userId;
	var userid = req.get('app_user_id') ;
	var data = req.body.data;
	if ( data.name != undefined) {
			/*{data:{'name':""}}*/
		var obj = {
			"name": data.name
		};
		users.update({'_id':id},obj,false,function (err,doc){
			if (err && doc == null) {
				res.send({'status':'error','data':["invalid user id and name"]});
			}else{				
				res.send({'status':'success','data':obj});
			}
		});
	}else{
		res.send({'status':'error','data':["invalid user id"]});
	}
	
});
function validateEmail(email) 
{
    var re = /\S+@\S+\.\S+/;
    return re.test(email);
}
user.put('/:userId/email',function (req , res, next){
	var id = req.get('userId') || req.params.userId;
	var userid = req.get('app_user_id') || req.headers.app_user_id;
	var data = req.body.data;
	if ( data.email != undefined && validateEmail(data.email)) {
			/*{data:{'email':""}}*/
		var obj = {
			"email": data.name
		};
		users.update({'_id':id},obj,false,function (err,doc){
			if (err && doc == null) {
				res.send({'status':'error','data':["invalid user id and email"]});
			}else{				
				res.send({'status':'success','data':obj});
			}
		});
	}else{
		res.send({'status':'error','data':["invalid user id or email"]});
	}
	
});
/* user modifications */
// user.put('/:userId',function (req , res, next){
// 	var id = req.get('userId') || req.params.userId;
// 	var userid = req.get('app_user_id') || req.headers.app_user_id;
// 	var data = req.body;
// 	if (userid == id) {
// 			/*{ id:"",data = [{"fieldName":"",fieldValue:""},{"fieldName":"",fieldValue:""}]*/
// 		var obj = {};
// 		for (var i = 0; i < data.data.length; i++) {
// 			obj[data.data[i].fieldName] = data.data[i].fieldValue;
// 		};
// 		users.update({'_id':id},obj,false,function (err,doc){
// 			console.log(err+'---'+doc);
// 			if (err) {
// 				res.send({'status':'error','data':["invalid user id"]});
// 			}else{				
// 				res.send({'status':'success','data':obj});
// 			}
// 		});
// 	}else{
// 		res.send({'status':'error','data':["invalid user id"]});
// 	}
	
// });


/* user booking */
user.post('/:userId/bookings',function (req , res, next){
	var id = req.params.userId;
	var userid = req.get('app_user_id') ;
	var data = req.body;
	/*{"referral_id":""}*/
	var coupon = '';
	if (data.promocode)
		coupon = cc.validate(data.promocode);
	var obj ={
		'user_id':id,
		'provider_id':data.provider_id,
		'service_id':data.service_id,
		'bookingDate':Date.now(),
		'slot'		:data.slot,
		'serviceDate':data.serviceDate || Date.now(),
		'totalPrice':data.totalPrice || 0,
		'offerPrice':data.offerPrice || 0,
		'isReferral':data.isReferral || false,
		'promocode': coupon != '' ? coupon : "NA",
		'status':data.status,
		'transaction_id':"HH"+Date.now(),
		'referral_id':data.referral_id
		};
		userbookings.save(obj,function (err,doc){
			if(err){
				var obj = {status:"error",data:["invalid data"]};
				res.send(obj);
			}else{
				var obj = {status:"success",data:doc};
				res.send(obj);
				
			}
		});
});
user.put('/:userId/bookings/:bookingId/:status',function (req , res, next){
	var id = req.get('userId') || req.params.userId;
	var userid = req.get('app_user_id') || req.headers.app_user_id;
	var bookingId = req.get('bookingId') || req.params.bookingId;
	var status = req.get('status') || req.params.status;
	var data = req.body;
	/*	{
		status = cancel/reschedule/remove,
		reschedule then need a date {'serviceDate':date}
	}
	*/
	if ( bookingId != undefined && status != undefined && status != null) {
		var obj = {};
		switch (status) {
		    case "cancel":
		        obj['status'] = "cancel";
		        break;
		    case "remove":
		        obj['isRemoved'] = true;
		        break;
		    // case "reschedule":
		    //     obj['serviceDate'] = data.serviceDate;
		    //     break;
		}
		if (underscore.isEmpty(obj)) {
				res.send({'status':'error','data':["invalid operation"]});
		}else{
			userbookings.update({'_id':bookingId,'isRemoved':false},obj,false,function (err,doc){
				if(err)
					res.send({'status':'error','data':["invalid booking id"]});
				else
					res.send({'status':'success','data':obj});
			});
		}
	}else{
		res.send({'status':'error','data':["invalid user id"]});
	}
});
user.get('/:userId/bookings/:bookingId',function (req , res, next){ 
	var bookingId = req.get('bookingId') || req.params.bookingId;
	var id = req.get('userId') || req.params.userId;
	userbookings.findRef({'_id':bookingId,'isRemoved':false},"service_id provider_id",{},{},0,1,function (err,doc){
		var obj = {'status':'success','data':doc};
		if (err) {
			obj.status = "error";
			obj.data = ["invalid user input"];
		}else{
			obj.status = "success";
			obj.data = doc;
		}
		res.send(obj);
	})
});
user.get('/:userId/bookings',function (req , res, next){ 
	var skip =  parseInt( req.query.skip) || 0;
	var limit = parseInt( req.query.limit) || 20;	
	var id = req.get('app_user_id') || req.headers.app_user_id;
		userbookings.find({'user_id':id,'isRemoved':false},{},{serviceDate:1},skip,limit,function (err,doc){
			var obj = {'status':'success','data':doc};
			if (err) {
				obj.status = "error";
				obj.data = ["invalid user input"];
			};
			if (!err && doc == null) {
				obj.status = "success";
				obj.data = {};				
			};
			res.send(obj);
		});
});
/* get related locations */
/* in query params latitude, longitude */
user.get('/providers',function (req , res, next){ 
	var skip =  parseInt( req.query.skip) || 0;
	var limit = parseInt( req.query.limit) || 20;
	var latitude =  req.query.latitude; 
	var longitude = req.query.longitude;
	providers.geoSearch(latitude,longitude,skip,limit,function (err,docs){
		if (err) {
			console.log('geoSearch errMsg : '+err);
			res.send({'status':'error','data':["No Service provider found"]});
		}else{
			var obj = {'status':'success','data':docs};
			if (docs == null) {
				obj.data = [];
			};
			res.send(obj);
		}
	});
});
user.get('/providers/:providerId',function (req , res, next){ 
	var providerId =  req.params.providerId;
		providers.findOne({'_id':providerId,'isRemoved':false,'isBlocked':false},{},function (err,doc){
			if (err) {
				res.send({'status':'error','data':["No Service provider found"]});
			}else{
				var obj = {'status':'success','data':doc};
				if (doc == null)
					obj.data = {};
				res.send(obj);
			}
		});
});
user.get('/:providerId/Services',function (req , res, next){ 
	var providerId = req.get('providerId') || req.params.providerId;
	var skip =  parseInt( req.query.skip) || 0;
	var limit = parseInt( req.query.limit) || 20;
		services.find({'provider_id':providerId,'isRemoved':false,'isExpired':false},{},{},skip,limit,function (err,docs){
			if (err) {
				res.send({'status':'error','data':["No Services found"]});
			}else{
				var obj = {'status':'success','data':docs};
				if (docs == null) {
					obj.data = [];
				};
				res.send(obj);
			}
		});
});
user.get('/:providerId/Services/:serviceId',function (req , res, next){ 
	var providerId = req.get('providerId') || req.params.providerId;
	var serviceId = req.get('serviceId') || req.params.serviceId;
		services.findOne({'_id':serviceId,'isRemoved':false},{},function (err,doc){
			if (err) {
				console.log('providers get by id : '+err);
				res.send({'status':'error','data':["No Services found"]});
			}else{
				var obj = {'status':'success','data':doc};
				if (doc == null) {
					obj.data = {};
				};
				res.send(obj);
			}
		});
});

user.post('/:providerId/Services/:serviceId/:service_Id/feedback',function (req , res, next){ 
	var providerId =  req.params.providerId;
	var serviceId =  req.params.serviceId;
	var service_Id = req.params.service_Id;
	var userid = req.get("app_user_id");
	var data = req.body;
	/* { rating: 1/2/3/4/5}*/
	var obj = {
		'user_id'		:userid,
		'provider_id'	:providerId,
		'booking_id'	:service_Id,
		'service_id'	:serviceId,
		'isRemoved'		:false,
		'rating'		:data.rating,
		'description'	:data.description,
		'feedbackDate'	:Date.now,
		'serviceDate'	:data.serviceDate
	};
	feedbacks.save(obj,function(err,doc){
		if (err) {
			res.send({'status':'error','data':["invalid object creation"]});
		}else{
			providers.findOne({'_id':providerId},{},function(err,docx){
				if (!err && docx != null) {
					var totalRatings = (docx.rating * docx.ratingCount)+data.rating;
					var rating = Math.round(totalRatings/(docx.ratingCount+1));
					providers.update({'_id':providerId},{'rating':rating},false,function(err,doc){
						console.log(providerId+" rating update "+rating);
					});
					providers.count({'_id':providerId},{'ratingCount':1},false,function(err,doc){
						console.log(providerId+" ratingCount update "+rating);
					});
				}
			});
			res.send({'status':'success','data':{}});
		}
	})
});

user.get('/:userId/favorites',function (req , res, next){ 
	var userId = req.params.userId 	
	var skip =  parseInt( req.query.skip) || 0;
	var limit = parseInt( req.query.limit) || 20;
		userfavourites.findRef({'_id':userId,'isRemoved':false},"provider_id",{},{},skip,limit,function (err,docs){
			if (err || docs == null) {
				console.log('providers get by id : '+err);
				res.send({'status':'error','data':["No favorites found"]});
			}else{
				var obj = {'status':'success','data':docs};
				if (docs == null) {
					obj.data = {};
				};
				res.send(obj);
			}
		});
});
user.post('/:userId/favorites',function (req , res, next){ 
	var userId = req.params.userId ;
	var skip =  parseInt( req.query.skip) || 0;
	var limit = parseInt( req.query.limit) || 20;
	var data = req.body.data;
	/*{data:{"provider_id":""}}*/
	userfavourites.findOne({'_id':userId,'isRemoved':false,'provider_id':data.provider_id},{},function (err,doc){
		if (err) {
			console.log('providers get by id : '+err);
			res.send({'status':'error','data':["No Services found"]});
		}else{ 
			
			if (doc == null) {
				var obj = {
						'user_id'		:userId,
						'provider_id'	:data.provider_id,
						'priority'		:data.priority
				};
				userfavourites.save(obj,function (err,docx){
					if (err) {
						var obj = {'status':'error','data':["already existed"]};
						res.send(obj);	
					}else{
						var obj = {'status':'success','data':docx};
						res.send(obj);
					}
				})
			}else{
				var obj = {'status':'error','data':["already existed"]};
				res.send(obj);
			}
		}
	});
});
user.put('/:userId/favorites/:favId',function (req , res, next){ 
	var userId = req.params.userId ;
	var favId = req.params.favId;	
	var body = req.body.data;
	var obj = {
		'priority':data.priority
	}
		userfavourites.update({'_id':favId},obj,false,function (err,docs){
			if (err) {
				res.send({'status':'error','data':["something went wrong"]});
			}else{
				var obj = {'status':'success','data':obj};
				res.send(obj);
			}
		});
});
user.delete('/:userId/favorites/:favId',function (req , res, next){ 
	var userId = req.params.userId ;
	var favId = req.params.favId;
	var obj = {
		'isRemoved':true
	};
		userfavourites.update({'_id':favId},obj,false,function (err,docs){
			if (err) {
				res.send({'status':'error','data':["something went wrong"]});
			}else{
				var obj = {'status':'success','data':obj};
				res.send(obj);
			}
		});
});
user.post('/:userId/generate/promo',function (req , res, next){ 
	var userId = req.params.userId ;
	 generateCouponCode(function (coupon){
		var data = req.body;
		var oneday = 86400000;
		var toDate = oneday * globals.promoDuration ;
		/* {"data":{"type":"single/multi","provider_id":"","service_id":"","category":"saloon/fitness"}}*/
		var obj = {
		'promocode'		:coupon,
		'fromDate'		:Date.now(),
	    'toDate'		:Date.now() + toDate,
		'user_id'		:userId,
		'provider_id'	:data.provider_id,
		'service_id'	:data.service_id,
		'type'			:data.type, // multi / single
		'isPercentage'	:globals.promoPercentage,
		'price'			:globals.promoPrice,
		'category'		:data.category //saloon or fitness
		};
		userpromos.save(obj,function (err,doc){
			if (err) {
				res.send({'status':'error',data:["something went wrong try again"]});
			}else{
				res.send({status:'success',data:doc});
			}
		});
	});
	
});

user.get('/:userId/referral',function (req , res, next){ 
	var userId = req.params.userId 	
	var skip =  parseInt( req.query.skip) || 0;
	var limit = parseInt( req.query.limit) || 20;
		userreferrals.findRef({'_id':userId,'isRemoved':false ,'isUsed':false},"provider_id service_id",{},{},skip,limit,function (err,docs){
			if (err) {
				console.log('providers get by id : '+err);
				res.send({'status':'error','data':["No favorites found"]});
			}else{
				var obj = {'status':'success','data':docs};
				if (docs == null) {
					obj.data = {};
				};
				res.send(obj);
			}
		});
});
user.put('/:userId/referral/:referral_id/used',function (req , res, next){ 
	var userId = req.params.userId ;
	var referral_id = req.params.referral_id;
	var obj = {isUsed:true};
	userreferrals.update({'_id':referral_id},obj,false,function (err,docs){
		if (err) {
			res.send({'status':'error','data':["something went wrong"]});
		}else{
			var obj = {'status':'success','data':obj};
			res.send(obj);
		}
	});
});
user.delete('/:userId/referral/:referral_id',function (req , res, next){ 
	var userId = req.params.userId ;
	var referral_id = req.params.referral_id;
	var obj = {isExpired:true,isRemoved:true};
	userreferrals.update({'_id':referral_id},obj,false,function (err,docs){
		if (err) {
			res.send({'status':'error','data':["something went wrong"]});
		}else{
			var obj = {'status':'success','data':obj};
			res.send(obj);
		}
	});
});

function validatePromo(doc,obj){
	if (doc.isFirst) {
		userpromos.findOne({'promocode':obj.usedReferralCode,'isExpired':false,'isRemoved':false},{},function (err,docx){
			if (!err && docx) {
				var current = Date.now();
				var offerFrom = new Date(docx.fromDate).getTime();
				var offerTo = new Date(docx.toDate).getTime();
				if (current > offerFrom && current < offerTo) {
					var toDate = Date.now() + (globals.promoUseDuration * 86400000);
					var refObj = {
						user_id:doc._id,
						ref_user_id:docx.user_id,
						promocode:docx.promocode,
						provider_id:docx.provider_id,
						service_id:docx.service_id,
						isReferral: true,
						isBackReferral: globals.isBackReferral,
						fromDate:Date.now(),
						toDate:toDate
					};
					userreferrals.save(refObj,function (err, docz){
						if (!err && docz) {
							users.update({'_id':doc._id},{'isFirst':false},false,function (err,resp){
								console.log("error: "+err+" --- isFirst updated for "+doc._id)
							});
						}
					})
				}else{
					userpromos.update({'_id':docx._id},{'isExpired':true,'isRemoved':true},false,function (err,result){
						console.log(docx.promocode +"-- Expired --& --err:"+err);
					});
				} 
			}
		});
	}
}
function generateOtp(){
	var otp;
	while(true){
		otp = Math.floor((Math.random() * 10000) + 1);
		if (otp.toString().length == 4) {
			break;
		}
	}
	return otp;
}
function generateJWT(id,callback){
	var token = jwt.sign({ 'id':id }, require('../utils/globals').secret,{ algorithm: 'HS256',expiresIn: '100 days' });
	callback(token);
}
function generateCouponCode(callback){
	var coupon;
	function loop(coupon){
		users.findOne({'referralCode':coupon},{"_id":1},function (err,doc){
			if (!err) {
				if(doc == null){ 
					callback (coupon);
				}else{
					coupon = cc.generate({ parts : 3 });
					loop(coupon);
				}
			}
		});
	}
	coupon = cc.generate({ parts : 3 });
	loop(coupon);
}
module.exports = user;
