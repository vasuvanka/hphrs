var express = require('express');
var underscore = require('underscore');
// var crypto = require('crypto');
var db = require('../model/schema');
var usersModel = require('../model/users').Users;
var UserBookingsModel = require('../model/userbookings').UserBookings;
var ServicesModel = require('../model/services').Services;
var ProvidersModel = require('../model/providers').Providers;
var UserPromosModel = require('../model/userpromos').UserPromos;

var uid = require('uid');
var jwt = require('jsonwebtoken'); 
var multer = require("multer");
var bcrypt = require('bcryptjs');

var provider = express.Router();

var users = new usersModel(db);
var userbookings = new UserBookingsModel(db);
var services = new ServicesModel(db);
var userpromos = new UserPromosModel(db);
var providers = new ProvidersModel(db);


provider.post('/login', function(req, res, next) {
	/* {"mobile":"","password"}*/
	var data = req.body;
	if ( data.mobile == undefined || data.mobile == null || data.password == null || data.password == undefined) {
		var obj = {status:"error",data:["invalid mobile Number or password"]};
		res.send(obj);
	}else{
		if (data.mobile.toString().trim().length == 10) {
			users.findOne({'mobile':data.mobile,'type':'provider'},{},function (err,doc){
				if (err || doc == null) {
					var obj = {status:"error",data:["provider does not exist"]};
					res.send(obj);
				}else{
					bcrypt.hash(data.password, doc.salt, function(err, hash) {
						if (underscore.isEqual(hash,doc.hash)) {
						    	generateJWT(doc._id,function (token){
					 				var objx = {'token':token,'lastLogin':Date.now()};
									users.update({'mobile':data.mobile},objx,false,function (err,docx){
										if (err) {
											var obj = {status:"error",data:["invalid mobile Number or password"]};
											res.send(obj);
										}else{
											var obj ={
												'id':doc._id,
												'name':doc.name,
												'mobile':doc.mobile,
												'pictureId':doc.pictureId,
												'token':token,
												'email':doc.email,
												'isBlocked':doc.isBlocked,
												'type':doc.type,
												'lastLogin':doc.lastLogin
											};
											res.send({'status':'success','data':obj});
										}
									});
								});
						}else{
							var obj = {status:"error",data:["invalid mobile Number or password"]};
							res.send(obj);
						}
					});
				}
			})
		}else{
			var obj = {status:"error",data:["invalid mobile Number"]};
			res.send(obj);
		}
	}
});
provider.post("/forgetPassword",function (req,res,next){
	// send otp to

	/* {"mobile":""}*/
	if ( req.body.mobile != undefined && req.body.mobile != null ) {
		if (req.body.mobile.toString().trim().length == 10) {
			users.findOne({'mobile':req.body.mobile,'type':'provider'},{},function (err,doc){
				if(err || doc == null){
					res.send({'status':'error','data':["invalid mobile number"]})
				}else{
					var otp = generateOtp();
					var objx = {'otp':otp.toString()};
					users.update({'_id':doc._id},objx,false,function (err,docx){
						console.log(err+'---'+JSON.stringify(docx));
						res.send({'status':'success','data':{"mobile":req.body.mobile,"otp" : otp}});
					});
				}
			})
		}else{
			var obj = {status:"error",data:["invalid mobile Number"]};
			res.send(obj);		
		}
	}else{
		var obj = {status:"error",data:["invalid mobile Number"]};
		res.send(obj);
	}
 
});

provider.post("/verifyotp",function (req,res,next){
	// send otp to

	/* {"mobile":"","otp":""}*/
	if ( req.body.mobile != undefined && req.body.otp != undefined ) {
		users.findOne({'mobile':req.body.mobile , 'type':'provider' , 'otp':req.body.otp},{},function (err,doc){
			if (err || doc == null) {
				var obj = {status:"error",data:["invalid mobile Number"]};
				res.send(obj);
			}else{
				generateJWT(doc._id,function(token){
					users.update({'_id':doc._id},{'token':token},false,function (err,docx){
						var objx = {'token':token,'id':doc._id};
						var obj = {status:"success",data:objx};
						if (err) { 
							obj.status = "error";
							obj.data = ["invalid mobile number"];
						};
						res.send(obj);
					})
				})
			}
		})
	}else{
		var obj = {status:"error",data:["invalid mobile Number"]};
		res.send(obj);
	}
 
});

function generateOtp(){
	var otp;
	while(true){
		otp = Math.floor((Math.random() * 10000) + 1);
		if (otp.toString().length == 4) {
			break;
		}
	}
	return otp;
}

/* provider headers validation */
provider.use(function (req, res, next) {
	var id = req.get('app_user_id') || null;
	var token = req.get('token') || null;
	if(!underscore.isNull(id) && !underscore.isUndefined(id) && !underscore.isNull(token) && !underscore.isUndefined(token)){
		next();
	}else{
		var err = new Error("Unauthorized Access or missing app_user_id, token");
		err.status = 404;
		next(err);
	}	
});
provider.use(function (req, res, next) {
	/* Bearer token*/
	var authToken = req.get('token');
	var token = authToken.split(" ")[1];
	jwt.verify(token,require('../utils/globals').secret, function(err, decoded) {
		if(!underscore.isUndefined(decoded) || !underscore.isUndefined(decoded.id)){
			 users.findOne({'_id':decoded.id,'token':token},{},function (err,doc){
			 	if (err || doc == null) {
					var err = new Error('Unauthorized Access');
					err.status = 401;
					next(err);
			 	}else{
			 		next();
			 	}
			 });
		}else{
			var err = new Error('Unauthorized Access');
			err.status = 401;
			next(err);
		}
	});
});

provider.post("/passwordChange",function (req,res,next){
	// send otp to
	var id = req.get('app_user_id')
	/* {"mobile":"","password":""}*/
	if ( req.body.mobile != undefined && req.body.password != undefined ) {
		users.findOne({'_id':id},{},function(err, docx) {
			if (!err && docx) {
				bcrypt.hash(req.body.password, docx.salt, function(err, hash) {
			        if (err) {
			        	res.send({'status':'error','data':['something went wrong']})
			        }else{
			        	var obj = {'password':req.body.password,'hash':hash,'lastPwdChange':Date.now()};
			        	users.update({'_id':id},obj,false,function (err,doc){
							if (err || doc == null) {
								var obj = {status:"error",data:["invalid mobile Number"]};
								res.send(obj);
							}else{
								var obj = {status:"success",data:{}};
								res.send(obj);
							}
						})
			        }
			    });
			}else{
				var obj = {status:"error",data:["something went wrong !!.."]};
				res.send(obj);
			}
		});
	}else{
		var obj = {status:"error",data:["invalid mobile Number"]};
		res.send(obj);
	}
 
});

/* providers list for a single owner */
provider.get('/providers',function (req , res, next){ 
	var skip =  parseInt(req.query.skip) || 0;
	var limit = parseInt(req.query.limit) || 20;	
	var id = req.get('app_user_id') ;
	var query = {'owner':id,'isRemoved':false};
	providers.find(query,{},{},skip,limit,function (err,doc){
		var obj = {'status':'success','data':doc};
		if (err) {
			obj.status = "error";
			obj.data = ["invalid provider input"];
		};
		if (!err && doc == null) {
			obj.status = "success";
			obj.data = [];				
		};
		res.send(obj);
	});
});
	/*file upload*/
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './files/uploads')
  },
  filename: function (req, file, cb) {
    cb(null, req.get('app_user_id') + Date.now())
  }
});
	 
var upload = multer({ storage: storage });


provider.get('/:ownerId/profile', function (req, res, next) {
	var id = req.get('app_user_id');
	var ownerId = req.params.ownerId;
	users.findOne({'_id':id},{"pictureId":1},function (err,doc){
			var obj = {};
			if (err && doc == null) {
				obj.status = "error";
				obj.data = ["invalid provider id"];
			}else{
				var data = { "pictureId" : doc.pictureId };
				obj.status = "success";
				obj.data = data;
			}
			res.send(obj);
	});
});

provider.post('/:ownerId/profile', upload.single('file'), function (req, res, next) {
	var ownerId = req.params.ownerId;
	var id = req.get('app_user_id');
	users.update({'_id':id},{ 'pictureId':req.file.filename },false,function (err,doc){
		var obj = {};
			if (err && doc == null) {
				obj.status = "error";
				obj.data = ["invalid provider id"];
			}else{
				var data = { "pictureId" : req.file.filename };
				obj.status = "success";
				obj.data = data;
			}
			res.send(obj);
	});

});

provider.post('/:providerId/profile/:serviceId', upload.single('file'), function (req, res, next) {
	var id = req.get('app_user_id');
	var serviceId = req.params.serviceId;
	var providerId = req.params.providerId ;
		services.update({'_id':serviceId},{'pictureId':req.file.filename},false,function (err,doc){
			var obj = {};
				if (err && doc == null) {
					obj.status = "error";
					obj.data = ["invalid provider id"];
				}else{
					var data = { "pictureId" : req.file.filename };
					obj.status = "success";
					obj.data = data;
				}
				res.send(obj);
		});
	

});
/* rename */
// function validateEmail(email) 
// {
//     var re = /\S+@\S+\.\S+/;
//     return re.test(email);
// }
// provider.put('/:providerId/email',function (req , res, next){
// 	var id = req.get('providerId') || req.params.providerId;
// 	var providerId = req.get('app_user_id') || req.headers.app_user_id;
// 	var data = req.body.data;
// 	if (providerId == id && data.email != undefined && validateEmail(data.email)) {
// 			/*{data:{'email':""}}*/
// 		var obj = {
// 			"email": data.email
// 		};
// 		providers.update({'_id':id},obj,false,function (err,doc){
// 			if (err) {
// 				res.send({'status':'error','data':["invalid provider id and email"]});
// 			}else{				
// 				res.send({'status':'success','data':obj});
// 			}
// 		});
// 	}else{
// 		res.send({'status':'error','data':["invalid provider id or email"]});
// 	}
	
// });
/* provider modifications */
// provider.put('/:providerId',function (req , res, next){
// 	var id = req.get('providerId') || req.params.providerId;
// 	var providerId = req.get('app_user_id') || req.headers.app_user_id;
// 	var data = req.body;
// 	if (providerId == id) {
// 			/*{ id:"",data = [{"fieldName":"",fieldValue:""},{"fieldName":"",fieldValue:""}]*/
// 		var obj = {};
// 		for (var i = 0; i < data.data.length; i++) {
// 			obj[data.data[i].fieldName] = data.data[i].fieldValue;
// 		};
// 		providers.update({'_id':id},obj,false,function (err,doc){
// 			console.log(err+'---'+doc);
// 			if (err) {
// 				res.send({'status':'error','data':["invalid provider id"]});
// 			}else{				
// 				res.send({'status':'success','data':obj});
// 			}
// 		});
// 	}else{
// 		res.send({'status':'error','data':["invalid provider id"]});
// 	}
	
// });


/* provider services */
provider.get('/:providerId/bookings/:status',function (req , res, next){ 
	var skip =  parseInt(req.query.skip) || 0;
	var limit = parseInt(req.query.limit) || 20;	
	var id = req.get('app_user_id') ;
	var providerId = req.params.providerId ;
	var status = req.params.status ;
	var query = {'provider_id':providerId,'isRemoved':false};
	switch(status){
		case "pending":
			query['status'] = "pending";
			break;
		case "completed":
			query['status'] = "completed";
			break;
		case "cancelled":
			query['status'] = "cancelled";
			break;
	}
	userbookings.find(query,{"transaction_id":1,"totalPrice":1,"slot":1,"serviceDate":1},{serviceDate:1},skip,limit,function (err,doc){
		var obj = {'status':'success','data':doc};
		if (err) {
			obj.status = "error";
			obj.data = ["invalid provider input"];
		};
		if (!err && doc == null) {
			obj.status = "success";
			obj.data = [];				
		};
		res.send(obj);
	});
});

provider.post('/:providerId/services',function (req , res, next){
	var providerId = req.params.providerId;
	var id = req.get('app_user_id');
	var data = req.body;
	/* {"name":"","isOffer":"",
		"provider_id":"","description":"","offerPrice":12,"totalPrice":21,
		"isPercentage":"","serviceType":"","isCombo":false,"offerList":[],"fromDate":""
		"toDate":"","isOnlyToday":true/false,"slot":[],"isEveryWeekDay":false
		"offerDayList":["monday","tuesday"],"isEveryDay":true/false
	} */
	
		var obj ={
			'name'			:data.name,
			'provider_id'	:providerId,
			'description'	:data.description,
			'totalPrice'	:data.totalPrice,
			'serviceType'	:data.serviceType,
			'isOffer'		:data.isOffer,
			'isPercentage'  :data.isPercentage,
			'offerPrice'	:data.offerPrice,
			'isEveryDay'	:data.isEveryDay,
			'isOnlyToday'	:data.isOnlyToday,
			'isEveryWeekDay':data.isEveryWeekDay,
			'fromDate'		:data.fromDate,
			'toDate'		:data.toDate,
			'slot'			:underscore.isString(data.slot)? JSON.parse(data.slot) :data.slot,
			'offerDayList'	:underscore.isString(data.offerDayList)? JSON.parse(data.offerDayList) :data.offerDayList,
			'pictureId'		:data.pictureId || "NA",
			'isCombo'		:data.isCombo,
			'offerList'		:underscore.isString(data.offerList)? JSON.parse(data.offerList) :data.offerList
		};
		services.save(obj,function (err,doc){
			if(err){
				var obj = {status:"error",data:["invalid data input"]};
				res.send(obj);
			}else{
				var obj = {status:"success",data:doc};
				res.send(obj);
			}
		});
	
});

provider.get('/:providerId/services',function (req , res, next){ 
	var providerId = req.params.providerId;
	var skip =  parseInt(req.query.skip) || 0;
	var limit = parseInt(req.query.limit) || 20;
	var serviceType = parseInt(req.query.type) || "saloon";
		services.find({'provider_id':providerId,'isRemoved':false,'serviceType':serviceType},{},{},skip,limit,function (err,docs){
			if (err) {
				res.send({'status':'error','data':["No Services found"]});
			}else{
				var obj = {'status':'success','data':docs};
				if (docs == null) {
					obj.data = [];
				};
				res.send(obj);
			}
		});
});
provider.get('/:providerId/services/:serviceId',function (req , res, next){ 
	var providerId = req.params.providerId;
	var serviceId = req.params.serviceId;
		services.findOne({'_id':serviceId,'isRemoved':false},{},function (err,doc){
			if (err) {
				console.log('providers get by id : '+err);
				res.send({'status':'error','data':["No Services found"]});
			}else{
				var obj = {'status':'success','data':doc};
				if (doc == null) {
					obj.data = {};
				};
				res.send(obj);
			}
		});
});
provider.put('/:providerId/services/:serviceId',function (req , res, next){
	var id = req.params.providerId;
	var providerId = req.get('app_user_id');
	var data = req.body.data;
	var serviceId = req.params.serviceId;
	/*{data:[{"name":"","value":""}]}*/
	if (data.length > 0) {
		var obj = {};
		for(var i = 0 ; i < data.length ; i++ ){
			obj[data[i].name] = data[i].value;
		};
		services.update({'_id':serviceId},obj,false,function (err,doc){
			if(err || doc == null){
				var obj = {status:"error",data:["invalid serviceId"]};
				res.send(obj);
			}else{
				var obj = {status:"success",data:obj};
				res.send(obj);
			}
		});
	}else{
		res.send({'status':'error','data':["invalid operation"]});
	}
});
provider.delete('/:providerId/services/:serviceId',function (req , res, next){
	var id = req.params.providerId;
	var providerId = req.get('app_user_id');
	var serviceId = req.params.serviceId;
	if (serviceId) {
		var obj = {"isRemoved":true};
		services.update({'_id':serviceId},obj,false,function (err,doc){
			if(err || doc == null){
				var obj = {status:"error",data:["invalid serviceId"]};
				res.send(obj);
			}else{
				var obj = {status:"success",data:obj};
				res.send(obj);
			}
		});
	}else{
		res.send({'status':'error','data':["invalid provider id"]});
	}
});

provider.put('/:providerId/bookings/:bookingId/completed',function (req , res, next){
	var providerId = req.get('app_user_id') ;
	var bookingId =  req.params.bookingId;
	var data = req.body;
	if (bookingId != undefined ) {
		var obj = {'status':'completed'};
		userbookings.update({'_id':bookingId,'isRemoved':false},obj,false,function (err,doc){
				if(err)
					res.send({'status':'error','data':["invalid booking id"]});
				else{
					providers.count({'_id':providerId},{serviceCount:1},function(err,doc){
						if (err) {
							console.log('err serviceCount ++ '+err);
						}
					});
					res.send({'status':'success','data':obj});
					applyReferal(bookingId);
				}
			});
	}else{
		res.send({'status':'error','data':["invalid provider id"]});
	}
});
provider.get('/:providerId/bookings/:bookingId',function (req , res, next){ 
	var bookingId = req.get('bookingId') || req.params.bookingId;
	var id = req.get('providerId') || req.params.providerId;
	userbookings.findOne({'_id':bookingId},{"transaction_id":1,"totalPrice":1,"slot":1,"serviceDate":1},function (err,doc){
		var obj = {'status':'success','data':doc};
		if (err) {
			obj.status = "error";
			obj.data = ["invalid provider input"];
		};
		if ( doc == null) 
			obj.data = {};				
		res.send(obj);
	});
});
provider.get('/:providerId/offers',function (req , res, next){ 
	var providerId = req.params.providerId;
	var skip =  parseInt(req.query.skip) || 0;
	var limit = parseInt(req.query.limit) || 20;
	var fromDate = req.query.fromDate || null;
	var toDate = req.query.toDate || null;
	if (underscore.isDate(fromDate) && underscore.isDate(toDate)) {
		var obj = {'provider_id':providerId,'isRemoved':false,'isOffer':true ,fromDate:{$gte:fromDate} ,toDate:{$lt:toDate}};
		services.find(obj,{},{},skip,limit,function (err,docs){
			if (err) {
				res.send({'status':'error','data':["No Services found"]});
			}else{
				var obj = {'status':'success','data':docs};
				if (docs == null) {
					obj.data = [];
				};
				res.send(obj);
			}
		});
	}else{
		services.find({'provider_id':providerId,'isRemoved':false,'isOffer':true},{},{},skip,limit,function (err,docs){
			if (err) {
				res.send({'status':'error','data':["No Services found"]});
			}else{
				var obj = {'status':'success','data':docs};
				if (docs == null) {
					obj.data = [];
				};
				res.send(obj);
			}
		});
	}
});
/*


*/

function applyReferal(bookingId){
	userbookings.findRef({'_id':bookingId,'promocode':{$ne:"NA"}},"referral_id",{},{},0,1,function (err,doc){
		if (!err && doc) {
			var obj = doc.referral_id[0];
			if (obj.isReferral && obj.isBackReferral) {
				var toDate = Date.now() + (globals.promoUseDuration * 86400000);
				var refObj = {
					ref_user_id:doc.user_id,
					user_id:obj.ref_user_id,
					promocode:"NA",
					provider_id:doc.provider_id,
					service_id:doc.service_id,
					isReferral: true,
					isBackReferral: false,
					fromDate:Date.now(),
					toDate:toDate
				};
				userreferrals.save(refObj,function (err, docz){
					if (!err && docz) {
						console.log("back referral done");
					}
				});
			}
		}
	})
}

function generateJWT(id,callback){
	var token = jwt.sign({ 'id':id }, require('../utils/globals').secret,{ algorithm: 'HS256',expiresIn: '100 days' });
	callback(token);
}
module.exports = provider;
